package me.demo.points.android.framework.log

import me.demo.points.core.interactors.Logger
import timber.log.Timber

class TimberLogger: Logger {
    override fun log(msg: String) {
        Timber.d(msg)
    }
}