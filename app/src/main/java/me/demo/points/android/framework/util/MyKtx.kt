package me.demo.points.android.framework.util

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.View
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.channels.actor

var View.visible: Boolean
    get() = this.visibility == View.VISIBLE
    set(value) {
        this.visibility = if (value) View.VISIBLE else View.GONE
    }

fun Fragment.displayErrorMsg(msg: String) {
    context?.let {
        Toast.makeText(it, msg, Toast.LENGTH_SHORT).show()
    }
}

fun View.onClick(action: suspend (View) -> Unit) {
    // launch one actor
    val eventActor = GlobalScope.actor<View>(Dispatchers.Main) {
        for (event in channel) action(event)
    }
    // install a listener to activate this actor
    setOnClickListener {
        eventActor.offer(it)
    }
}

fun EditText.toInt(): Int = try {
    text.toString().toInt()
} catch (e: Exception) {
    Int.MIN_VALUE
}

inline fun <reified T: Activity> Context.createIntent() =
    Intent(this, T::class.java)

inline fun <reified T: Activity> Fragment.startActivity() {
    startActivity(activity?.createIntent<T>())
}

fun Fragment.getColor(@ColorRes colorId: Int): Int =
    ContextCompat.getColor(activity as Context, colorId)

fun Double.to2DecimalsString(): String = "%.2f".format(this)