package me.demo.points.android.framework

import android.app.Application
import me.demo.points.android.BuildConfig
import me.demo.points.android.framework.di.mainModule
import me.demo.points.android.framework.di.networkModule
import me.demo.points.android.framework.di.pointsModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class PointsApp: Application() {

    override fun onCreate() {
        super.onCreate()
        initTimber()
        initKoin()
    }

    private fun initTimber(){
        if (BuildConfig.DEBUG){
            Timber.plant(Timber.DebugTree())
        }
    }

    private fun initKoin() = startKoin {
        androidContext(this@PointsApp)
        modules(listOf(mainModule, pointsModule, networkModule))
    }

}