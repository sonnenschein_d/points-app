package me.demo.points.android.presentation.main

import androidx.fragment.app.Fragment
import me.demo.points.android.framework.points.ParcelablePoint
import me.demo.points.android.framework.util.createIntent
import me.demo.points.android.presentation.points.PointsActivity
import me.demo.points.core.domain.Point
import me.demo.points.core.interactors.IMainNavigator

class MainNavigator(val fragment: Fragment) : IMainNavigator {
    override fun goToPoints(points: List<Point>) {
        fragment.activity?.let{activity ->
            activity.startActivity(activity.createIntent<PointsActivity>()
                .apply {
                    val list = ArrayList(points.map { p -> ParcelablePoint(p) })
                    this.putParcelableArrayListExtra(PointsActivity.EXTRA_POINTS, list)
                })
        }
    }
}