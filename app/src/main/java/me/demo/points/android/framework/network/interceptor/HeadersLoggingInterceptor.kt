package me.demo.points.android.framework.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response
import okio.Buffer
import timber.log.Timber

class HeadersLoggingInterceptor: Interceptor{
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()

        val t1 = System.nanoTime()
        Timber.d(
            String.format(
                "--> Sending request %s on %s%n%s",
                request.url(),
                chain.connection(),
                request.headers()
            )
        )

        val requestBuffer = Buffer()
        request.body()?.writeTo(requestBuffer)
        Timber.d(requestBuffer.readUtf8())

        val response = chain.proceed(request)

        val t2 = System.nanoTime()
        Timber.d(
            String.format(
                "<-- Received response for %s in %.1fms%n%s",
                response.request().url(),
                (t2 - t1) / 1e6,
                response.headers()
            )
        )

        return response
    }
}