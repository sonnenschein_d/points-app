package me.demo.points.android.framework.network

import me.demo.points.core.data.ResultDataSource
import me.demo.points.core.domain.PointsListResult

class RetrofitResultDataSource(private val api: PointsService): ResultDataSource {
    override suspend fun fetch(count: Int): PointsListResult = api.fetchPoints(count)
}