package me.demo.points.android.framework.util

import android.util.Base64
import me.demo.points.core.interactors.Decoder
import java.nio.charset.StandardCharsets

class AndroidBase64Decoder : Decoder {
    override fun decode(msg: String): String {
        val data = Base64.decode(msg, Base64.DEFAULT)
        return String(data, StandardCharsets.UTF_8)
    }
}