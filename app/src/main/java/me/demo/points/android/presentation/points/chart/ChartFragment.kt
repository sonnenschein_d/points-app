package me.demo.points.android.presentation.points.chart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.formatter.IFillFormatter
import me.demo.points.android.R
import me.demo.points.android.framework.util.getColor
import me.demo.points.android.presentation.points.PointsViewModel
import me.demo.points.core.domain.Point
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ChartFragment : Fragment() {

    companion object {
        fun newInstance() = ChartFragment()
    }

    private lateinit var chart: LineChart

    private val viewModel: PointsViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.points_chart_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        setupChart()
        observePoints()
    }

    private fun initViews(view: View) {
        chart = view.findViewById(R.id.chart)
    }

    private fun setupChart() {
        setupXAxis()
        setupYAxis()
        setupDetails()
    }

    private fun setupXAxis() = chart.xAxis.apply {
        isEnabled = true
        setLabelCount(6, false)
        textColor = getColor(R.color.chartXAxisText)
        axisLineColor = getColor(R.color.chartXAxis)
        setDrawGridLines(false)
        position = XAxis.XAxisPosition.BOTTOM
    }

    private fun setupYAxis() = chart.axisLeft.apply {
        setLabelCount(6, false)
        textColor = getColor(R.color.chartYAxisText)
        setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)
        setDrawGridLines(false)
        axisLineColor = getColor(R.color.chartYAxis)
    }

    private fun setupDetails() = chart.apply {
        setViewPortOffsets(150f, 100f, 100f, 150f)
        description.isEnabled = false
        setTouchEnabled(true)
        isDragEnabled = true
        setScaleEnabled(true)
        setPinchZoom(true)
        setDrawGridBackground(false)
        axisRight.isEnabled = false
        legend.isEnabled = false
        animateXY(1000, 1000)
        invalidate()
    }

    private fun observePoints() {
        viewModel.getPoints().observe(viewLifecycleOwner, Observer<List<Point>> {
            val data = it.map { p -> Entry(p.x.toFloat(), p.y.toFloat()) }

            if (chart.hasData()) {
                chart.updateData(data)
            } else {
                val set = newSet(data)
                val lineData = newLineData(set)
                chart.data = lineData
            }
        })
    }

    private fun newSet(data: List<Entry>): LineDataSet =
        LineDataSet(data, getString(R.string.points_chart_dataset_lbl)).apply {
            mode = LineDataSet.Mode.CUBIC_BEZIER
            cubicIntensity = 0.2f
            setDrawFilled(true)
            setDrawCircles(true)
            lineWidth = 1.8f
            circleRadius = 4f
            setCircleColor(this@ChartFragment.getColor(R.color.chartCircle))
            highLightColor = this@ChartFragment.getColor(R.color.chartHighlight)
            color = this@ChartFragment.getColor(R.color.chartLine)
            fillColor = this@ChartFragment.getColor(R.color.chartFill)
            fillAlpha = resources.getInteger(R.integer.chart_fill_alpha)
            setDrawHorizontalHighlightIndicator(false)
            fillFormatter =
                IFillFormatter { dataSet, dataProvider -> chart.axisLeft.axisMinimum }
        }

    private fun newLineData(set: LineDataSet): LineData = LineData(set).apply {
        setValueTextSize(9f)
        setDrawValues(false)
    }

}

private fun LineChart.hasData(): Boolean = data != null && data.dataSetCount > 0
private fun LineChart.updateData(values: List<Entry>) {
    val set = data.getDataSetByIndex(0) as LineDataSet
    set.values = values
    data.notifyDataChanged()
    notifyDataSetChanged()
}