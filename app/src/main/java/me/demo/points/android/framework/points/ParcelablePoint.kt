package me.demo.points.android.framework.points

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import me.demo.points.core.domain.Point

@Parcelize
data class ParcelablePoint(val x: Double, val y: Double): Parcelable {
    constructor(point: Point):this(point.x, point.y)
}