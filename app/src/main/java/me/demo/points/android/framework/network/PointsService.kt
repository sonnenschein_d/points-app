package me.demo.points.android.framework.network

import me.demo.points.core.domain.PointsListResult
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface PointsService {

    @FormUrlEncoded
    @POST("/mobws/json/pointsList")
    suspend fun fetchPoints(@Field("count") count: Int,
                    @Field("version") version: String = "1.1"): PointsListResult

}