package me.demo.points.android.presentation.points

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import me.demo.points.android.R
import me.demo.points.android.framework.points.ParcelablePoint
import me.demo.points.android.presentation.points.chart.ChartFragment
import me.demo.points.android.presentation.points.table.TableFragment
import me.demo.points.core.domain.Point
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.KoinComponent

class PointsActivity : AppCompatActivity(), KoinComponent {

    private val viewModel: PointsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.points_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.table, TableFragment.newInstance())
                .replace(R.id.graph, ChartFragment.newInstance())
                .commitNow()
            initPoints()
        }
    }

    private fun initPoints() = viewModel.setPoints(intent.getPoints())

    private fun Intent.getPoints(): List<Point> =
        getParcelableArrayListExtra<ParcelablePoint>(EXTRA_POINTS).map { Point(it.x, it.y) }

    companion object {
        const val EXTRA_POINTS = "points"
    }

}
