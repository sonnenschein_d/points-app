package me.demo.points.android.presentation.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import me.demo.points.core.domain.Point
import me.demo.points.core.domain.isSuccess
import me.demo.points.core.interactors.FetchResult

class MainViewModel(private val fetch: FetchResult) : ViewModel() {

    sealed class PointsState{
        object Loading: PointsState()
        object Empty: PointsState()
        data class Success(val points: List<Point>) : PointsState()
        data class Error(val code: Int, val msg: String) : PointsState()
    }

    private val state = MutableLiveData<PointsState>().apply {
        this.value = PointsState.Empty
    }

    fun state(): LiveData<PointsState> = state
    fun clearError(){
        state.value = PointsState.Empty
    }

    fun fetchPoints(count: Int) = viewModelScope.launch {
        state.postValue(PointsState.Loading)
        try {
            val res = fetch(count)
            if (res.isSuccess()){
                state.postValue(PointsState.Success(res.points))
            } else {
                state.postValue(PointsState.Error(res.result, res.msg))
            }
        } catch (e: Exception) {
            state.postValue(PointsState.Error(Int.MAX_VALUE, e.localizedMessage))
        }
    }

}
