package me.demo.points.android.presentation.points

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import me.demo.points.core.domain.Point
import me.demo.points.core.interactors.SortPoints

class PointsViewModel(private val sort: SortPoints) : ViewModel() {

    private val points: MutableLiveData<List<Point>> = MutableLiveData()

    fun setPoints(points: List<Point>){
        this.points.postValue(sort(points))
    }

    fun getPoints(): LiveData<List<Point>> = points

}
