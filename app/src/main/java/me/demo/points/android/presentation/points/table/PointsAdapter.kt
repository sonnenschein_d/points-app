package me.demo.points.android.presentation.points.table

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import me.demo.points.android.R
import me.demo.points.core.domain.Point

class PointsAdapter : RecyclerView.Adapter<PointViewHolder>(){

    private var items: List<Point> = emptyList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PointViewHolder {
        val inflatedView = LayoutInflater.from(parent.context)
            .inflate(R.layout.points_table_row, parent, false)
        return PointViewHolder(inflatedView)
    }

    override fun getItemCount(): Int = items.size + 1

    override fun onBindViewHolder(holder: PointViewHolder, position: Int) = if (position == 0){
        bindHeader(holder)
    } else {
        holder.bind(position, items[position - 1])
    }

    private fun bindHeader(holder: PointViewHolder) = holder.bind("#", "x", "y")

    fun update(items: List<Point>){
        if (this.items != items){
            this.items = items
            notifyDataSetChanged()
        }
    }

}

