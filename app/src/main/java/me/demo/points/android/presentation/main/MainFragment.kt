package me.demo.points.android.presentation.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import me.demo.points.android.R
import me.demo.points.android.framework.util.displayErrorMsg
import me.demo.points.android.framework.util.onClick
import me.demo.points.android.framework.util.toInt
import me.demo.points.android.framework.util.visible
import me.demo.points.core.domain.Point
import me.demo.points.core.interactors.IMainNavigator
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import timber.log.Timber

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var count: EditText
    private lateinit var go: Button
    private lateinit var progressBar: ProgressBar

    private val viewModel: MainViewModel by viewModel()
    private val navigator: IMainNavigator by inject { parametersOf(this) }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        observeState()
        setupGo()
    }

    private fun observeState(){
        viewModel.state().observe(viewLifecycleOwner, Observer<MainViewModel.PointsState> {
            onPointsStateChanged(it)
        })
    }

    private fun onPointsStateChanged(state: MainViewModel.PointsState){
        when(state){
            is MainViewModel.PointsState.Empty -> {}
            is MainViewModel.PointsState.Loading -> displayLoading(true)
            is MainViewModel.PointsState.Success -> onSuccess(state.points)
            is MainViewModel.PointsState.Error -> onError(state.code, state.msg)
        }
    }

    private fun displayLoading(loading: Boolean){
        go.isEnabled = !loading
        progressBar.visible = loading
    }

    private fun onSuccess(points: List<Point>){
        displayLoading(false)
        navigator.goToPoints(points)
    }

    private fun onError(code: Int, msg: String){
        Timber.d("Error: code = $code, msg = $msg")
        displayLoading(false)
        displayErrorMsg(msg)
        viewModel.clearError()
    }

    private fun initViews(view: View){
        count = view.findViewById(R.id.count)
        go = view.findViewById(R.id.go)
        progressBar = view.findViewById(R.id.progressBar)
    }

    private fun setupGo(){
        go.onClick { view -> viewModel.fetchPoints(count.toInt()) }
    }

}
