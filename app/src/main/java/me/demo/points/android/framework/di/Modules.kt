package me.demo.points.android.framework.di

import android.content.Context
import androidx.fragment.app.Fragment
import com.google.gson.GsonBuilder
import me.demo.points.android.R
import me.demo.points.android.framework.log.TimberLogger
import me.demo.points.android.framework.network.PointsService
import me.demo.points.android.framework.network.ResultDeserializer
import me.demo.points.android.framework.network.RetrofitResultDataSource
import me.demo.points.android.framework.util.AndroidBase64Decoder
import me.demo.points.android.presentation.main.MainNavigator
import me.demo.points.android.presentation.main.MainViewModel
import me.demo.points.android.presentation.points.PointsViewModel
import me.demo.points.android.presentation.points.table.PointsAdapter
import me.demo.points.core.data.ResultDataSource
import me.demo.points.core.data.ResultRepository
import me.demo.points.core.domain.PointsListResult
import me.demo.points.core.interactors.*
import okhttp3.OkHttpClient
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.BufferedInputStream
import java.security.KeyStore
import java.security.cert.CertificateFactory
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManagerFactory
import javax.net.ssl.X509TrustManager


val mainModule = module {

    factory { (fragment: Fragment) ->
        MainNavigator(fragment) as IMainNavigator
    }
    viewModel { MainViewModel(get()) }

}

val pointsModule = module {

    viewModel { PointsViewModel(get()) }
    factory { PointsAdapter() }
    factory { SortPoints() }

}

val networkModule = module {

    factory { AndroidBase64Decoder() as Decoder }
    factory { DecodeResult(get()) }

    factory { TimberLogger() as Logger }
    factory { LogResult(get()) }

    factory { RetrofitResultDataSource(get()) as ResultDataSource }
    factory { ResultRepository(get()) }
    factory { FetchResult(get(), get()) }

    single {
        GsonBuilder()
            .registerTypeAdapter(PointsListResult::class.java, ResultDeserializer())
            .create()
    }

    single {
        GsonConverterFactory.create(get()) as Converter.Factory
    }


    single {

        val sslContext: SSLContext = SSLContext.getInstance("TLS")
        val keyStore = KeyStore.getInstance(KeyStore.getDefaultType())
        keyStore.load(null, null)
        val ctx: Context = get()
        val certInputStream = ctx.resources.openRawResource(R.raw.server)
        val bis = BufferedInputStream(certInputStream)
        val certificateFactory = CertificateFactory.getInstance("X.509")
        while (bis.available() > 0) {
            val cert = certificateFactory.generateCertificate(bis)
            keyStore.setCertificateEntry("demo.bankplus.ru", cert)
        }
        val trustManagerFactory =
            TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm())
        trustManagerFactory.init(keyStore)
        val trustManagers = trustManagerFactory.getTrustManagers()
        sslContext.init(null, trustManagers, null)


        OkHttpClient.Builder()
            .sslSocketFactory(sslContext.getSocketFactory(), trustManagers[0] as X509TrustManager)
            .build()
    }

    single {
        Retrofit.Builder()
            .baseUrl("https://demo.bankplus.ru/")
            .addConverterFactory(get())
            .client(get())
            .build().create(PointsService::class.java)
    }
}