package me.demo.points.android.framework.network

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.google.gson.JsonParseException
import me.demo.points.core.domain.Point
import me.demo.points.core.domain.PointsListResult
import java.lang.reflect.Type


class ResultDeserializer : JsonDeserializer<PointsListResult> {
    override fun deserialize(
        json: JsonElement?,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): PointsListResult {
        val root = json?.getAsJsonObject() ?: fail("Null response")
        val response = root.get("response")?.asJsonObject ?: fail("Null response element")
        //seems that "result" is in the root of json object
        //when everything is ok & in "response" element when some errors occurred
        //(for example, when parameters are wrong)
        //it's probably some server side error, but let's process both variants
        //to be on the safe side.
        val result = (root.get("result")?.asInt ?: response.get("result")?.asInt) ?: fail("No result element")
        val message = response.get("message")?.asString ?: ""

        val points = response.get("points")?.asJsonArray?.asIterable()?.map {
            val p = it.asJsonObject
            Point(p.get("x").asDouble, p.get("y").asDouble)
        }

        return PointsListResult(result, points ?: emptyList(), message)
    }

    private fun fail(msg: String): Nothing = throw JsonParseException(msg)

}