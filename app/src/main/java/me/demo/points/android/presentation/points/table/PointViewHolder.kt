package me.demo.points.android.presentation.points.table

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import me.demo.points.android.R
import me.demo.points.android.framework.util.to2DecimalsString
import me.demo.points.core.domain.Point

class PointViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    private val index: TextView = view.findViewById(R.id.index)
    private val x: TextView = view.findViewById(R.id.x)
    private val y: TextView = view.findViewById(R.id.y)

    fun bind(index: Int, point: Point) =
        bind(
            index.toString(),
            point.x.to2DecimalsString(),
            point.y.to2DecimalsString()
        )

    fun bind(index: String, x: String, y: String) {
        this.index.text = index
        this.x.text = x
        this.y.text = y
    }

}