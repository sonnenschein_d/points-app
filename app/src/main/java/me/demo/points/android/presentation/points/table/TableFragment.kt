package me.demo.points.android.presentation.points.table

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import me.demo.points.android.R
import me.demo.points.android.presentation.points.PointsViewModel
import me.demo.points.core.domain.Point
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class TableFragment : Fragment() {

    companion object {
        fun newInstance() = TableFragment()
    }

    private lateinit var list: RecyclerView
    private val adapter: PointsAdapter by inject()

    private val viewModel: PointsViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.points_table_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initViews(view)
        setupTable()
        observePoints()
    }

    private fun initViews(view: View) {
        list = view.findViewById(R.id.list)
    }

    private fun setupTable() {
        list.layoutManager = LinearLayoutManager(activity)
        list.setHasFixedSize(true)
        list.adapter = adapter
    }

    private fun observePoints() {
        viewModel.getPoints().observe(viewLifecycleOwner, Observer<List<Point>> {
            adapter.update(it)
        })
    }

}
