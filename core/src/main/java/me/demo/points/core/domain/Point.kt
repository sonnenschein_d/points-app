package me.demo.points.core.domain

data class Point(val x: Double, val y: Double)