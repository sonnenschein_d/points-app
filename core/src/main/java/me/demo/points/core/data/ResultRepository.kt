package me.demo.points.core.data

import me.demo.points.core.domain.PointsListResult

class ResultRepository(private val dataSource: ResultDataSource) {
    suspend fun fetchResult(count: Int): PointsListResult = dataSource.fetch(count)
}