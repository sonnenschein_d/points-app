package me.demo.points.core.data

import me.demo.points.core.domain.PointsListResult

interface ResultDataSource {
    suspend fun fetch(count: Int): PointsListResult
}