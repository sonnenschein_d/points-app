package me.demo.points.core.interactors

import me.demo.points.core.domain.PointsListResult

class DecodeResult(private val decoder: Decoder) {
    operator fun invoke(result: PointsListResult): PointsListResult = with(result){
        if (this.result == 0 || this.result == -100){
            return@with this
        }
        val msg = try {
            decoder.decode(this.msg)
        } catch (e: Exception) {
            this.msg
        }
        return@with PointsListResult(this.result, this.points, msg)
    }
}