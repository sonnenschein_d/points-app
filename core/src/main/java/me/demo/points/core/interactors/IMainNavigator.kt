package me.demo.points.core.interactors

import me.demo.points.core.domain.Point

interface IMainNavigator {
    fun goToPoints(points: List<Point>)
}