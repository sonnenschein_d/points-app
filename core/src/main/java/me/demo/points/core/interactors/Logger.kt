package me.demo.points.core.interactors

interface Logger {
    fun log(msg: String)
}