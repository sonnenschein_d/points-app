package me.demo.points.core.domain

data class PointsListResult(val result: Int, val points: List<Point>, val msg: String)