package me.demo.points.core.interactors

interface Decoder {
    fun decode(msg: String): String
}