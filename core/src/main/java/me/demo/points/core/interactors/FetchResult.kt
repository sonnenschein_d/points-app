package me.demo.points.core.interactors

import me.demo.points.core.data.ResultRepository
import me.demo.points.core.domain.PointsListResult

class FetchResult(private val repository: ResultRepository,
                  private val decode: DecodeResult) {
    suspend operator fun invoke(count: Int): PointsListResult{
        val res = repository.fetchResult(count)
        return decode(res)
    }
}