package me.demo.points.core.domain

import java.lang.StringBuilder

fun PointsListResult.isSuccess(): Boolean = result == 0
fun PointsListResult.toLog(): String = if (result == 0){
    points.toLog()
} else {
    "result = $result, msg = $msg"
}

fun Point.toLog(): String = "x = $x, y = $y"
fun List<Point>.toLog(): String{
    val builder = StringBuilder()
    forEach { builder.appendln(it.toLog()) }
    return builder.toString()
}