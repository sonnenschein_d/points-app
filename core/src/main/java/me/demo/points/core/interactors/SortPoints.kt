package me.demo.points.core.interactors

import me.demo.points.core.domain.Point

class SortPoints {
    operator fun invoke(points: List<Point>): List<Point> = points.sortedWith(
        Comparator { p0, p1 ->
            p0.x.compareTo(p1.x)
        }
    )
}