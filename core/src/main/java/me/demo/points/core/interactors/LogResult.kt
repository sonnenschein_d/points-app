package me.demo.points.core.interactors

import me.demo.points.core.domain.PointsListResult
import me.demo.points.core.domain.toLog

class LogResult(private val logger: Logger) {
    operator fun invoke(result: PointsListResult) = logger.log(result.toLog())
}